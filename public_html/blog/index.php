<?php
$pageTitle = 'Blog';
$pageDescription = 'Personal Blog of';

include '../inc/loader.php';
include '../inc/header.php';
include '../inc/nav.php';
include 'fcn/parsedown.php';
$Parsedown = new Parsedown();
?>
    <section class="container page-start">
        <div class="row">
            <div class="col">
                <h1 class="page-header text-center">Blog</h1>
            </div>
        </div>
        <hr>
        <?php
        $dir = new DirectoryIterator(dirname('../blog/posts/*'));
        foreach ($dir as $fileinfo) {
            if (!$fileinfo->isDot()) {
                $post = file_get_contents('../blog/posts/' . $fileinfo->getFilename());
                ?>
                <div class="row">
                    <div class="col mx-auto text-center">
                        <?= $Parsedown->text($post); ?>
                    </div>
                </div>

                <?php
            }
        }
        ?>
        <div class="row">
            <div class="col mx-auto text-center">
                <h2>Coming Soon ...</h2>
            </div>
        </div>
    </section>
<?php
include '../inc/footer.php';
