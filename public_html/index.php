<?php
$pageTitle = 'Maxwell Power - Full Stack Developer';
$pageDescription = 'Personal Website of';
include 'inc/loader.php';
include 'inc/header.php';
include 'inc/nav.php';
?>
    <!-- Cover -->
    <header id="cover" class="cover">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12 text-center">
                    <div class="row">
                        <div class="col">
                            <h1 class="display-1">I am:</h1>
                            <h2 class="display-3">Maxwell Power</h2>
                            <h3 class="display-5">Full Stack Developer/Telecom Engineer/Champion of Customer
                                Success</h3>
                        </div>
                    </div>
                    <div id="profile_links" class="row">
                        <div class="col">
                            <div class="btn-group flex-wrap" role="group" aria-label="Profile Links">
                                <button class="btn btn-large btn-secondary"
                                        onclick="window.open('//www.facebook.com/maxtpower')"
                                        title="Facebook">
                                    <i class="fab fa-facebook fa-3x"></i>
                                </button>
                                <!--<button class="btn btn-large btn-secondary"
                                        onclick="window.open('//www.instagram.com/maxwelltpower')"
                                        title="Instagram">
                                    <i class="fab fa-instagram fa-3x"></i>
                                </button>-->
                                <button class="btn btn-large btn-secondary"
                                        onclick="window.open('//www.twitter.com/maxtpower')"
                                        title="Twitter">
                                    <i class="fab fa-twitter fa-3x"></i>
                                </button>
                                <!--<button class="btn btn-large btn-secondary"
                                        onclick="window.open('//www.tiktok.com/@maxtpower')"
                                        title="Twitter">
                                    <i class="fab fa-tiktok fa-3x"></i>
                                </button>-->
                                <button class="btn btn-large btn-secondary"
                                        onclick="window.open('//www.linkedin.com/in/maxwellpower')"
                                        title="Linkedin">
                                    <i class="fab fa-linkedin fa-3x"></i>
                                </button>
                                <button class="btn btn-large btn-secondary"
                                        onclick="window.open('//github.com/maxwellpower')"
                                        title="GitHub">
                                    <i class="fab fa-github fa-3x"></i>
                                </button>
                                <button class="btn btn-large btn-secondary"
                                        onclick="window.open('//gitlab.com/mpower')"
                                        title="GitLab">
                                    <i class="fab fa-gitlab fa-3x"></i>
                                </button>
                                <button class="btn btn-large btn-secondary"
                                        onclick="window.open('//calendly.com/maxpower/coffee')"
                                        title="Calendar">
                                    <i class="fa-solid fa-calendar fa-3x"></i>
                                </button>
                                <button class="btn btn-large btn-secondary"
                                        onclick="window.open('//www.dropbox.com/request/cuDlgUFhGEVj0l73YJ6U')"
                                        title="Dropbox Upload">
                                    <i class="fab fa-dropbox fa-3x"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="payment_links" class="mt-3 row">
                        <div class="col">
                            <div class="btn-group flex-wrap" role="group" aria-label="Payment Links">
                                <button type="button" class="btn btn-lg btn-secondary"
                                        onclick="window.open('//www.buymeacoffee.com/maxp')"
                                        title="Buy Me a Coffee">
                                    <i class="fa-solid fa-coffee"></i>
                                </button>
                                <button type="button" class="btn btn-lg btn-secondary"
                                        onclick="window.open('//www.patreon.com/maxtpower')"
                                        title="Patreon">
                                    <i class="fab fa-patreon"></i>
                                </button>
                                <button type="button" class="btn btn-large btn-secondary"
                                        onclick="window.open('//www.paypal.me/maxtpower')"
                                        title="PayPal">
                                    <i class="fab fa-cc-paypal fa-2x"></i>
                                </button>
                                <button type="button" class="btn btn-large btn-secondary"
                                        onclick="window.open('//www.venmo.com/maxtpower')"
                                        title="Venmo">
                                    <i class="fa-solid fa-money-bill fa-2x"></i>
                                </button>
                                <button type="button" class="btn btn-large btn-secondary"
                                        onclick="window.open('//cash.app/$M1XP3R')"
                                        title="Cash App">
                                    <i class="fa-solid fa-money-bill fa-2x"></i>
                                </button>
                                <button type="button" class="btn btn-large btn-secondary"
                                        onclick="window.open('//www.blockchain.com/btc/address/13SPrxkZEuCs9kUhUq7KNKQGJ5Jdxf7n2S')"
                                        title="Bitcoin">
                                    <i class="fab fa-bitcoin fa-2x"></i>
                                </button>
                                <button type="button" class="btn btn-large btn-secondary"
                                        onclick="window.open('//www.amazon.ca/hz/wishlist/ls/3TN63GE5KZSLI')"
                                        title="Amazon Wish List">
                                    <i class="fab fa-amazon fa-2x"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </header>

    <!-- About -->
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h3>Highly organized and talented professional with more than ten years experience working remotely
                        and more than twenty-five years of Information Technology, Software Development, DevSecOps,
                        Management, and Telecommunications experience. </h3>
                    <hr class="small mx-auto">
                    <p class="lead">Offering technical expertise in Data Centres, IPv4 and IPv6 Data Networks,
                        Telecom/VoIP, Storage, Databases, Application Development, Desktop and Server Hardware/Software
                        Support.</p>
                    <p>Additional areas of expertise include delivering internal and external technology projects,
                        managing human and material resources, developing detailed budgets and proposals, forecasting,
                        planning, and disaster recovery—a results-driven leader with superior motivational skills and
                        the ability to work and manage collaboratively or independently both remote and onsite. Proven
                        ability to efficiently prioritize, consistently meet deadlines, and effectively evaluate and
                        generate solutions to complex internal and external challenges.</p>
                </div>
            </div>
        </div>
    </section>

<?php
include 'inc/footer.php';
