<!-- Nav -->
<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
    <div class="container">
        <a class="navbar-brand text-center" href="/">Maxwell Power</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-nav-dropdown"
                aria-controls="navbar-nav-dropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar-nav-dropdown">
            <ul class="navbar-nav ms-auto">
                <li class="<?= ($_SERVER['PHP_SELF'] === '/index.php') ? 'nav-item active' : 'nav-item'; ?>">
                    <a class="nav-link" href="/"><i class="fa-solid fa-home" aria-hidden="true"></i> Home</a>
                </li>
                <?php if ($_SERVER['PHP_SELF'] === '/blog/index.php') { ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="/blog"><i class="fa-solid fa-newspaper" aria-hidden="true"></i> Blog</a>
                    </li>
                <?php } ?>
                <li class="<?= ($_SERVER['PHP_SELF'] === '/portfolio.php') ? 'nav-item active' : 'nav-item'; ?>">
                    <a class="nav-link" href="/portfolio"><i class="fa-solid fa-briefcase" aria-hidden="true"></i>
                        Portfolio</a>
                </li>
                <li class="<?= ($_SERVER['PHP_SELF'] === '/resume.php') ? 'nav-item active' : 'nav-item'; ?>">
                    <a class="nav-link" href="/resume"><i class="fa-solid fa-file" aria-hidden="true"></i>
                        Resume</a>

                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/tree" target="_blank" data-instant><i class="fa-solid fa-tree"
                                                                                     aria-hidden="true"></i>
                        Tree</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa-solid fa-tools" aria-hidden="true"></i> Tools
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="//subnet.mtp.xyz" rel="noopener" target="_blank" data-instant><i
                                    class="fa-solid fa-network-wired"></i>
                            Subnet Cheat Sheet</a>
                    </div>
                </li>
                <li class="<?= ($_SERVER['PHP_SELF'] === '/contact.php') ? 'nav-item active' : 'nav-item'; ?>"><a
                            class="nav-link" href="/contact"><i class="fa-solid fa-envelope" aria-hidden="true"></i>
                        Contact</a></li>
            </ul>
        </div>
    </div>
</nav>
<?php
// Messages
if (isset($_SESSION['messages'])) {
    echo '<div id="system-messages" class="container margin-top-40"><br><div class="col-md-8 col-12 mx-auto"><a href="#" class="close" data-dismiss="system-messages">&times;</a><strong>', $_SESSION['messages'], '</strong></div></div>';
    unset($_SESSION['messages']);
}
