<?php
$pageDescriptionAppend = 'Maxwell Power - Full Stack Developer, Telecom Engineer, and Champion of Customer Success.';

if (!isset($pageTitle)) {
    $pageTitle = 'Maxwell Power - Full Stack Developer';
}
if (!isset($pageDescription)) {
    $pageDescription = 'Personal Website of';
}

?>
<!--
“The way to get started is to quit talking and begin doing.” – Walt Disney
-->
<!DOCTYPE html>
<html lang="en-ca">
<head>
    <title><?= ($_SERVER['PHP_SELF'] === '/index.php') ? $pageTitle : $pageTitle . ' • Maxwell Power'; ?></title>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="handheldfriendly" content="true">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="<?= "$pageDescription $pageDescriptionAppend"; ?>">
    <meta name="author" content="Maxwell Power">

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-00Q78ES3VS"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-00Q78ES3VS');
    </script>

    <!-- Open Graph -->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Personal Website of Maxwell Power">
    <meta property="og:title"
          content="<?= ($_SERVER['PHP_SELF'] === '/index.php') ? $pageTitle : $pageTitle . ' • Maxwell Power - Full Stack Developer'; ?>">
    <meta property="og:description" content="<?= "$pageDescription $pageDescriptionAppend"; ?>">
    <meta property="og:url" content="https://www.maxtpower.com">
    <meta property="og:image"
          content="https://www.maxtpower.com/img/share.jpg">

    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:description" content="<?= "$pageDescription $pageDescriptionAppend"; ?>">
    <meta name="twitter:title"
          content="<?= ($_SERVER['PHP_SELF'] === '/index.php') ? $pageTitle : $pageTitle . ' • Maxwell Power - Full Stack Developer'; ?>">
    <meta name="twitter:image" content="https://www.maxtpower.com/img/share.jpg">

    <!-- CSS -->
    <?php
    if (isset($_SERVER['HTTP_USER_AGENT']) && stripos($_SERVER['HTTP_USER_AGENT'], "chrome") > 0) { ?>
        <link rel="preload" href="../css/bootstrap/bootstrap.min.css" as="style"
              onload="this.onload=null;this.rel='stylesheet'">
        <link rel="preload" href="../css/default.css" as="style"
              onload="this.onload=null;this.rel='stylesheet'">
        <link rel="preload"
              href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CPatua+One%7CPermanent+Marker%7CModak%7CJosefin+Sans:700%7CPoppins&display=swap"
              as="style"
              onload="this.onload=null;this.rel='stylesheet'">
        <noscript>
            <link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css">
            <link rel="stylesheet" href="../css/default.css">
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CPatua+One%7CPermanent+Marker%7CModak%7CJosefin+Sans:700%7CPoppins&display=swap"
                  rel="stylesheet">
        </noscript>
    <?php } else { ?>
        <link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css">
        <link rel="stylesheet" href="../css/default.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CPatua+One%7CPermanent+Marker%7CModak%7CJosefin+Sans:700%7CPoppins&display=swap"
              rel="stylesheet">
    <?php } ?>

    <!-- favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="../img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../img/favicon/favicon-16x16.png">
    <link rel="manifest" href="../img/favicon/site.webmanifest">

    <?php if ($_SERVER['PHP_SELF'] === '/index.php') { ?>
        <!-- Structured Data -->
        <script type="application/ld+json">
            {
                "@context": "http://schema.org",
                "@type": "WebSite",
                "name": "Maxwell Power",
                "description": "<?= "$pageDescription $pageDescriptionAppend"; ?>",
                "url": "https://www.maxtpower.com"
            }
        </script>
        <script type="application/ld+json">
            {
                "@context": "http://schema.org",
                "@type": "Person",
                "@id": "https://www.maxtpower.com/#person",
                "address": {
                    "@type": "PostalAddress",
                    "addressLocality": "Edmonton",
                    "addressRegion": "AB",
                    "addressCountry": "Canada"
                },
                "givenName": "Maxwell",
                "additionalName": "Thomas",
                "familyName": "Power",
                "alternateName": "Max Power",
                "email": "hello@maxtpower.com",
                "birthPlace": {
                    "@type": "Place",
                    "address": {
                        "@type": "PostalAddress",
                        "addressLocality": "St.John's",
                        "addressRegion": "NF",
                        "addressCountry": "Canada"
                    }
                },
                "gender": "male",
                "nationality": "Canadian",
                "url": "https://www.maxtpower.com",
                "sameAs": [
                    "https://www.facebook.com/maxtpower",
                    "https://www.linkedin.com/in/maxwellpower",
                    "https://github.com/maxwellpower",
                    "https://www.pinterest.com/maxtpower",
                    "https://www.instagram.com/maxwelltpower",
                    "https://gitlab.com/mpower",
                    "https://gitlab.com/maxtpower",
                    "https://www.twitter.com/maxwelltpower",
                    "https://keybase.io/maxpower",
                    "https://www.buymeacoffee.com/maxp",
                    "https://www.paypal.me/maxtpower",
                    "https://www.maxwellpower.ca",
                    "https://www.maxpower.co"
                ],
                "image": "https://www.maxtpower.com/img/profile.jpg",
                "Description": "Full Stack Developer/Telecom Engineer/Champion of Customer Success",
                "knowsAbout": [
                    "PHP",
                    "Linux",
                    "NodeJS",
                    "JavaScript",
                    "Windows",
                    "Git",
                    "Telecom",
                    "Customer Success",
                    "Project Management",
                    "Account Management",
                    "Customer Service"
                ],
                "jobTitle": "Senior Customer Engineer",
                "worksFor": [
                    {
                        "@type": "Organization",
                        "name": "Mattermost",
                        "url": "https://www.mattermost.com"
                    }
                ],
                "owns": [
                    {
                        "name": "Acuparse",
                        "description": "AcuRite Access/smartHUB and IP Camera Processing, Display, and Upload.",
                        "url": "https://www.acuparse.com",
                        "sameAs": [
                            "https://gitlab.com/acuparse",
                            "https://github.com/acuparse",
                            "https://bitbucket.com/acuparse"
                        ]
                    }
                ],
                "knowsLanguage": [
                    "English",
                    "French"
                ],
                "height": "71 inches"
            }
        </script>
    <?php } ?>
</head>
<body>
