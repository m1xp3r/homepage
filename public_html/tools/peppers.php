<?php
$pageTitle = 'Spicy Peppers';
$pageDescription = 'Peppers by Scoville heat units';
include '../inc/loader.php';
include '../inc/header.php';
include '../inc/nav.php';
?>
    <section id="peppers" class="container page-start">
        <div class="row">
            <div class="col text-center">
                <img src="../img/other/hot_peppers.png">
            </div>
        </div>
    </section>
<?php
include '../inc/footer.php';
